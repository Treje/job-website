import React, {Component} from "react"
import {
    Link
  } from 'react-router-dom'
  //css

class Drawer extends Component{
    constructor(props){
        super(props)
        
    }
    toggleDrawer(){
        document.querySelector('.mdl-layout').MaterialLayout.toggleDrawer()
    }

    render(){
        return(
            <div className="mdl-layout__drawer">
                <span className="mdl-layout-title">Job Scout</span>
                <nav className="mdl-navigation">
                <Link onClick={this.toggleDrawer} className="mdl-navigation__link" to="/me">
                    <figure style={{margin:"auto"}}>
                        <img src="http://placehold.it/200x200" alt="user_img"  style={imgStyle}/><p> Name </p>
                        <figcaption style={{marginTop:"auto"}}><i className="material-icons">person</i> <p>User</p> </figcaption>
                    </figure>
                </Link>
                    <Link onClick={this.toggleDrawer} className="mdl-navigation__link" to="/">
                    <i className="material-icons">home</i><p>Home</p></Link>
                    <Link onClick={this.toggleDrawer} className="mdl-navigation__link" to="/Jobs">Job List</Link>
                    <Link onClick={this.toggleDrawer} className="mdl-navigation__link" to="/Companies">Companies</Link>
                    <Link onClick={this.toggleDrawer} className="mdl-navigation__link" to="/About">About</Link>
                </nav>
            </div>
        )
    }
}

let imgStyle = {
    width: "5vh",
    height: "5vh",
    margin: "auto",
    marginBottom: "10px",
    borderRadius: "75%",
    background: "var(--mdc-theme-primary)"
}

export default Drawer;