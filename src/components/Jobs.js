
import React,{Component} from "react";

import {Link} from "react-router-dom";
import MediaQuery from 'react-responsive';

class Jobs extends Component{

     
      constructor(props){
        super(props)
        this.state = {jobs: []} 
    
      }
      componentWillMount(){
          this.getJobs()
      }

      componentWillUnmount(){
        
        }
    
      getJobs(){
        if(this.state.jobs.length === 0){
            if(this.props.company){
                this.props.getJobs(this.props.company)
                .then(jobs => {
                    this.setState({jobs:jobs})
                })
            }
        }
       
            
      }
    
      
      formatJob(job,i){
       return  (<MediaQuery key={i} query="(min-width: 500px)">
        {
            (matches) => {
                if(matches){
                    return (
                        <div key={i} style={{width:"20vw",height:"10vh",margin:"2em"}} className="mdl-card mdl-shadow--4dp">
                        <div className="mdl-card__title">{job.title} - {job.location}</div>
                        <div className="mdl-card__supporting-text">{job.company}</div>
                        <div style={{marginTop:"auto"}}className="mdl-card__actions mdl-card--border">
                            <Link to={`/save`} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >Save</Link>
                            <a href={job.url} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >View Job</a> 
                        </div>
                        </div>
                    )
                }else{
                    return (  
                    <div key={i} style={{width:"80vw",height:"10vh",margin:"2em"}} className="mdl-card mdl-shadow--4dp">
                    <div className="mdl-card__title">{job.title} - {job.location}</div>
                    <div className="mdl-card__supporting-text">{job.company}</div>
                    <div style={{marginTop:"auto"}}className="mdl-card__actions mdl-card--border">
                        <Link to={`/save`} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >Save</Link>
                        <a href={job.url} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >View Job</a> 
                    </div>
                    </div>
                    )
                }

            }
        }
        </MediaQuery>)
      }
      jobList(){
        let html = []
       
        this.state.jobs.sort((a,b) => a.daysListed - b.daysListed).forEach((job,i) => {
            if(Array.isArray(job)){
                job.sort((a,b)=> a.daysListed - b.daysListed).forEach((a,j) => {
                    html.push(this.formatJob(a,a.title+a.company+a.location))
                })  
            }else{
                html.push(
                this.formatJob(job,i)
                )
            }
            
        })
        if(this.props.jobsWeek){

        
            this.props.jobsWeek.forEach(company => {
                company
                .sort((a,b)=> a.daysListed - b.daysListed)
                .filter(a => a.daysListed === 0)
                .forEach((job,i) => {
                    html.push(this.formatJob(job,job.title+job.company+job.location))
                })  
            })
        }

        return html;
      }
    
      render(){
        
        return (
          <div className="page-content" style={{
            display:"flex",
            flexFlow:"row",
            flexGrow:1,
            flexWrap: "wrap",
            justifyContent:"center"
    
            }}>

          {this.jobList()}
          
          </div>
        )
      }
      
    }

export default Jobs
    