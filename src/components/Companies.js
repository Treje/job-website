import React,{Component} from "react";
import {Link} from "react-router-dom";
import MediaQuery from 'react-responsive';

class Companies extends Component{
    constructor(props){
      super(props)
      this.companies = ["a","b","c","d","e","f","g"]
    }
  
    companyList(){
      let html = []
      this.props.companies.forEach(title => {
        html.push(
            <MediaQuery key={title} minWidth={500}>
            {(matches) => {
                let encodedTitle = encodeURIComponent(title);
                if(matches){
                    return (
                        <div key={title} style={{width:"20vw",height:"10vh",margin:"2em"}} className="mdl-card mdl-shadow--4dp">
                        <div style={{textAlign: 'center', marginTop: 10}}>{title}</div>
                        <div style={{marginTop:"auto"}}className="mdl-card__actions mdl-card--border">
                        <Link to={`/${encodedTitle}/info`} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >Company Info</Link>
                        <Link to={`/${encodedTitle}/jobs`} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >View Jobs</Link> 
                        </div>
                    </div>
                    )
                }else{
                    return(
                        <div key={title} style={{width:"80vw",height:"10vh",margin:"2em"}} className="mdl-card mdl-shadow--4dp">
                        <div style={{textAlign: 'center', marginTop: 10}}>{title}</div>
                        <div style={{marginTop:"auto"}}className="mdl-card__actions mdl-card--border">
                        <Link to={`/${encodedTitle}/info`} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >Company Info</Link>
                        <Link to={`/${encodedTitle}/jobs`} className="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent" >View Jobs</Link> 
                        </div>
                    </div>
                    )
                }
            
            }}
            </MediaQuery>
        )
      })
      return html
    }
  
    render(){
      return(
        <div className="page-content" style={{
          display:"flex",
          flexFlow:"row",
          flexGrow:1,
          flexWrap: "wrap",
          justifyContent:"center"
  
          }}>
  
          {this.companyList()}
  
        </div>
      )
    }
  
  }
  export default Companies;