import React, { Component } from 'react';
import {
  Link,
  NavLink
} from 'react-router-dom'
import { CSSTransitionGroup} from 'react-transition-group';
import "./Header.css"

class Header extends Component{

    componentDidMount(){
        document.querySelector('header .mdl-navigation__link').style.setProperty('--secondary-color','yellow')
    }
    constructor(props){
        super(props);
        
    }

    render(){
        return(
        <header className="mdl-layout__header mdl-layout__header--scroll">
            <div className="mdl-layout__header-row">
                <span className="mdl-layout-title">Job Scout</span>

                <div className="mdl-layout-spacer"></div>
            
                <nav className="mdl-navigation">
                    <NavLink exact className="mdl-navigation__link" to="/" >Home</NavLink>
                    <NavLink className="mdl-navigation__link" to="/Jobs">Job List</NavLink>
                    <NavLink className="mdl-navigation__link" to="/Companies">Companies</NavLink>
                    <NavLink className="mdl-navigation__link" to="/About">About</NavLink>
                    <NavLink className="mdl-navigation__link" to="/Me">User</NavLink>
                </nav>
            </div>
        </header>
        )
    }
    

}

export default Header;
