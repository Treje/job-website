import React, { Component } from 'react';
//router
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
  Link,
  matchPath
} from 'react-router-dom';
//firebase
import firebase from "./firebase";
//components
import Header from "./components/Header";
import Drawer from "./components/Drawer";
//import Footer from "./components/Footer";
//Pages
import Companies from "./components/Companies";
import Jobs from "./components/Jobs";
import About from "./components/About";


//import materialize from "materialize-css";
import './App.css';


class App extends Component {
  constructor(props){
    super(props);
    this.db = {}
    this.state = {companies: [],jobWeek: []}
  }

  getNewJobs = async (opts) =>{
    //new jobs for this week
    let today = new Date().getDay()
    let data = []
    let option = 0
    console.time("New Jobs")
    if(this.state.companies.length == 0)
    {
      console.log("No companies")
      return data;
    }
    console.log("Found compainees")
    let companies = this.state.companies
   
    data = await Promise.all(companies.map(async company =>{
        let data = []
        let newJobs = this.db
                    .collection('company')
                    .doc(company)
                    .collection('jobs')
                    .where('daysListed','<=',today)
                  
        let snapshot = await newJobs.get()
        if(snapshot.empty){
            return data
        }
            snapshot.forEach(doc => {
                let docData = doc.data()
                console.log(docData)
                data.push(docData)
            })
        return data
    }))
    console.timeEnd("New Jobs")
    this.setState({jobWeek:data.filter(a => a.length > 0)})
  }

  getJobs = (from) => {
    let data = []
    let company =  this.db.collection('company').doc(from).collection('jobs')    
    return new Promise((resolve,reject) => company.get()
        .then(snapshot => {
            snapshot.forEach(doc => {
            //console.log(doc.id, '=>', doc.data())
            data.push(doc.data())
            });
           resolve(data)
        })
        .catch(err =>{
            reject(err.message)
            console.log('Error getting documents')
        }))
      
   
  }

  getCompanies = async () => {
    let companies = [];
    let snapshot = await this.db.collection("scrapers").get()
    
    snapshot.forEach(doc => {
      companies.push(doc.data().company)
    }); 
    return companies
    
  }

  componentWillMount(){
    this.db = firebase
    if(this.state.companies.length == 0){
      (async ()=>{
        let companies = await this.getCompanies()
        this.setState({companies:companies})
        await this.getNewJobs()
      })()
    }

  }

  render() {
    console.log(this.state.companies)
    
    return (
      <Router>
        <div className="App">
          <div className="mdl-layout mdl-js-layout">          
            <Header/>
            <Drawer/>
            
            <Route exact path="/" render={() => (<Jobs jobsWeek={this.state.jobWeek} />)}/>
            <Route path="/Jobs" render={() => (<Jobs jobsWeek={this.state.jobWeek} />) }/>
            <Route path="/Companies" render={ () => (<Companies companies={this.state.companies} />)}/>
            <Route path="/About" component={About}/>
            <Route path="/:company/jobs" render={(match) => {return(<Jobs company={match.match.params.company} getJobs={this.getJobs}/>)}}/>
          </div>
        </div>
      </Router>
    );

  }


}
const Home = () => {
  
  return (
    <div className="page-content">

    <section >
    
    </section>
    </div>
  )
}




export default App;
