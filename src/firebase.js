//firebase
import * as firebase from "firebase";
import 'firebase/firestore';

 // Initialize Firebase
 const config = {
    apiKey: "AIzaSyCH8WkT6qDsLfF_dZklLASkB8N8v58IuBo",
    authDomain: "jobscoutbb.firebaseapp.com",
    databaseURL: "https://jobscoutbb.firebaseio.com",
    projectId: "jobscoutbb",
    storageBucket: "jobscoutbb.appspot.com",
    messagingSenderId: "966538789765"
  };


  firebase.initializeApp(config);
  var db = firebase.firestore()
  export default db;
 // console.log(db)


